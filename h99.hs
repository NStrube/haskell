-- 99 Haskell Problems
-- https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems

import Control.Monad (replicateM)
import Data.List (tails, sortOn, sort, groupBy)

-- Problem 1
myLast :: [a] -> Maybe a
myLast [] = Nothing
myLast [x] = Just x
myLast (_:xs) = myLast xs

-- Problem 2
myButLast :: [a] -> Maybe a
myButLast [] = Nothing
myButLast [x,_] = Just x
myButLast (_:xs) = myButLast xs

-- Problem 3
elementAt :: [a] -> Int -> Maybe a
elementAt [] _ = Nothing
elementAt (x:_) 1 = Just x
elementAt (_:xs) n = elementAt xs $ n - 1

-- Problem 4
myLength :: [a] -> Int
myLength [] = 0
myLength (x:xs) = foldr (\x -> (+) 1) 0 xs

-- Problem 5
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

-- Problem 6
isPalindrome :: Eq a => [a] -> Bool
isPalindrome [] = True
isPalindrome xs = xs == reverse xs

-- Problem 7
data NestedList a = Elem a | List [NestedList a] deriving Show

flatten :: NestedList a -> [a]
flatten (Elem x) = [x]
flatten (List []) = []
flatten (List (x:xs)) = flatten x ++ flatten (List xs)

-- Problem 8
compress :: Eq a => [a] -> [a]
compress = compress' []
    where
        compress' lst [] = reverse lst
        compress' [] (x:xs) = compress' [x] xs
        compress' lst (x:xs) = compress' (if x == head lst then lst else x:lst) xs

-- Problem 9
pack :: Eq a => [a] -> [[a]]
pack xs = pack' [] xs
    where
        pack' lst [] = reverse lst
        pack' [] (x:xs) = pack' [[x]] xs
        pack' (l:ls) (x:xs) = pack' (if x == head l then (x:l):ls else [x]:l:ls) xs

-- Problem 10
encode :: Eq a => [a] -> [(Int, a)]
encode = map (\xs -> (length xs, head xs)) . pack

-- Problem 11
data RLE a = Single a | Multiple Int a deriving Show

encodeModified :: Eq a => [a] -> [RLE a]
encodeModified = map rle . pack
    where
      rle xs = let n = length xs
                   x = head xs in
               if n == 1 then Single x else Multiple n x

-- Problem 12
decodeModified :: [RLE a] -> [a]
decodeModified [] = []
decodeModified (Single x:xs) = x:decodeModified xs
decodeModified (Multiple n x:xs) = replicate n x ++ decodeModified xs

-- Problem 13
encodeDirect :: Eq a => [a] -> [RLE a]
encodeDirect = encodeDirect' []
    where
      encodeDirect' lst [] = reverse $ map (\(n, x) -> if n == 1 then Single x else Multiple n x) lst
      encodeDirect' [] (x:xs) = encodeDirect' [(1, x)] xs
      encodeDirect' ((n,l):ls) (x:xs) = encodeDirect' (if l == x then (n+1,l):ls else (1,x):(n,l):ls) xs

-- Problem 14
dupli :: [a] -> [a]
dupli [] = []
dupli (x:xs) = x:x:dupli xs

-- Problem 15
repli :: [a] -> Int -> [a]
repli [] _ = []
repli _ 0 = []
repli (x:xs) n = replicate n x ++ repli xs n

-- Problem 16
dropEvery :: [a] -> Int -> [a]
dropEvery xs n = dropEvery' n n xs
    where
      dropEvery' 0 _ _ = []
      dropEvery' _ _ [] = []
      dropEvery' n 1 (_:xs) = dropEvery' n n xs
      dropEvery' n k (x:xs) = x:dropEvery' n (k-1) xs

-- Problem 17
split :: [a] -> Int -> ([a], [a])
-- split xs n = (take n xs, drop n xs)
-- NOTE: For hlint
split = flip splitAt

-- Problem 18
slice :: [a] -> Int -> Int -> [a]
slice xs m n = take (n-m') $ drop m' xs
    where m' = m - 1

-- Problem 19
rotate :: [a] -> Int -> [a]
rotate xs n
    | n == 0 = xs
    | n > 0 = let xs' = take n xs in drop n xs ++ xs'
    | otherwise = let n' = (-n) in drop (length xs - n' + 1) xs ++ take n' xs

-- Problem 20
removeAt :: Int -> [a] -> [a]
removeAt _ [] = []
removeAt 1 (_:xs) = xs
removeAt n (x:xs) = x:removeAt (n-1) xs

-- Problem 21
insertAt :: a -> [a] -> Int -> [a]
insertAt y xs 1 = y:xs
insertAt y (x:xs) n = x:insertAt y xs (n-1)

-- Problem 22
range :: Int -> Int -> [Int]
range start end = if start == end then [end] else start:range (start + 1) end

-- NOTE: Skipping rng stuff

-- Problem 26
combinations :: Int -> [a] -> [[a]]
combinations 0 _ = [[]]
combinations _ [] = []
combinations n (x:xs) = map (x:) (combinations (n-1) xs) ++ combinations n xs

-- Problem 27
-- TODO:

-- Problem 28a
lsort :: [[a]] -> [[a]]
lsort = sortOn length

-- Problem 28b
-- Map (length -> frequency) -> sortOn (Map.get . length)
lfsort :: [[a]] -> [[a]]
lfsort = concat . lsort . groupBy (\xs ys -> length xs == length ys) . lsort

-- Problem 31
isPrime :: Int -> Bool
isPrime n = null $ [x | x <- [2..div n 2], mod n x == 0]

-- Problem 32
myGcd :: Int -> Int -> Int
myGcd 0 b = b
myGcd a 0 = a
myGcd a b = myGcd b $ mod a b

-- Problem 33
comprime :: Int -> Int -> Bool
comprime a b = 1 == myGcd a b

-- Problem 34
totient :: Int -> Int
totient x = length $ [y | y <- [1..(x-1)], comprime x y]

-- Problem 35
primeFactors :: Int -> [Int]
primeFactors x
    | isPrime x = [x]
    | otherwise =  d : primeFactors (div x d)
    where d = head [y | y <- [2..div x 2], mod x y == 0, isPrime y]

-- Problem 36
primeFactorsMult :: Int -> [(Int, Int)]
primeFactorsMult = map swap . encode . primeFactors
    where swap (x,y) = (y,x)

-- Problem 37
phi :: Int -> Int
phi x = product [(p-1)*p ^ (n-1) | (p,n) <- primeFactorsMult x]

-- Problem 39
primesR :: Int -> Int -> [Int]
primesR a b = [p | p <- [a..b], isPrime p]

-- Problem 40
goldbach :: Int -> (Int, Int)
goldbach n = head [(x,y) | x <- p, y <- p, x+y == n]
    where p = primesR 2 (n-2)

-- Problem 41
goldbachList :: Int -> Int -> [(Int, Int)]
goldbachList a b = map goldbach $ dropWhile (<4) $ filter even [a..b]

goldbachList' :: Int -> Int -> Int -> [(Int, Int)]
goldbachList' a b x = filter (\(y,z) -> y > x && z > x) $ goldbachList a b

-- Problem 46
and',or',nand',xor',impl',equ' :: Bool -> Bool -> Bool
and' = (&&)
or' = (||)
nor' a b = not $ or' a b
nand' a b = not $ and' a b
xor' a b = not $ equ' a b
-- impl' a b = or' (not a) b
impl' True b = b
impl' False _ = True
equ' = (==)

table :: (Bool -> Bool -> Bool) -> IO ()
table fn = mapM_ putStrLn [show a ++ " " ++ show b ++ " " ++ show (fn a b) | a <- [True, False], b <- [True,False]]

-- Problem 48
-- TODO: I do not understand how this solution works.
tablen :: Int -> ([Bool] -> Bool) -> IO ()
tablen n fn =  mapM_ putStrLn [show a ++ " " ++ " " ++ show (fn a) | a <- args n]
    where args n = replicateM n [True, False]

-- Problem 49
gray :: Int -> [String]
gray 0 = [""]
gray n = map ('0':) xs ++ map ('1':) (reverse xs)
    where xs = gray $ n - 1

data Tree a
    = Empty
    | Branch a (Tree a) (Tree a)
      deriving (Show, Eq)

leaf :: a -> Tree a
leaf x = Branch x Empty Empty

-- Problem 55
-- TODO:

-- Problem 56
mirror :: Tree a -> Tree a -> Bool
mirror Empty Empty = True
mirror (Branch _ l1 r1) (Branch _ l2 r2) = mirror l1 r2 && mirror r1 l2
mirror _ _ = False

symmetric :: Tree a -> Bool
symmetric Empty = True
symmetric (Branch _ l r) = mirror l r

-- Problem 57
construct :: [Int] -> Tree Int
construct [] = Empty
construct (x:xs) = Branch x (construct (filter (< x) xs)) (construct (filter (>= x) xs))

-- Problem 59
-- TODO:

-- Problem 60
-- TODO:

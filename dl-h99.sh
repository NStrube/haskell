#!/bin/sh

set -e

URL="https://wiki.haskell.org/99_questions"

mkdir -p h99

set -- "1_to_10" "11_to_20" "21_to_28" "31_to_41" "46_to_50" "54A_to_60"\
    "61_to_69" "70B_to_73" "80_to_89" "90_to_94" "95_to_99"

for p in "$@"; do curl -o "h99/$p.html" "$URL/$p"; done

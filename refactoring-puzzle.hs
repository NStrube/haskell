-- Refactoring puzzle
-- https://blog.tmorris.net/posts/refactoring-puzzle/

newtype IntRdr a =
  IntRdr {
    readIntRdr :: Int -> a
  }

instance Functor IntRdr where
    -- fmap :: (a -> b) -> f a -> f b
    fmap f (IntRdr g) = IntRdr $ f . g

instance Applicative IntRdr where
    pure = IntRdr . const
    -- (<*>) :: f (a -> b) -> f a -> f b
    (IntRdr f) <*> (IntRdr g) = IntRdr $ \n -> (f n (g n))

instance Monad IntRdr where
    -- (>>=) :: m a -> (a -> m b) -> m b
    (IntRdr g) >>= f = IntRdr $ \n -> readIntRdr (f (g n)) n

mapIntRdr ::
  IntRdr a
  -> (a -> b)
  -> IntRdr b
mapIntRdr (IntRdr g) f =
  IntRdr (f . g)

bindIntRdr ::
  IntRdr a
  -> (a -> IntRdr b)
  -> IntRdr b
bindIntRdr (IntRdr g) f =
  IntRdr (\n -> readIntRdr (f (g n)) n)

applyIntRdr ::
  a
  -> IntRdr a
applyIntRdr =
  IntRdr . const

type Option = Maybe

mapOption ::
  Option a
  -> (a -> b)
  -> Option b
mapOption Nothing _ =
  Nothing
mapOption (Just a) f =
  Just (f a)

bindOption ::
  Option a
  -> (a -> Option b)
  -> Option b
bindOption Nothing _ =
  Nothing
bindOption (Just a) f =
  f a

applyOption ::
  a
  -> Option a
applyOption a =
  Just a

-- Return all the Some values, or None if not all are Some.
runOptions :: [Option a] -> Option [a]
runOptions = foldr (\a b -> bindOption a (\aa -> mapOption b (aa:))) (applyOption [])

-- Apply an Int to a list of int readers and return the list of return values.
runIntRdrs :: [IntRdr a] -> IntRdr [a]
runIntRdrs = foldr (\a b -> bindIntRdr a (\aa -> mapIntRdr b (aa:))) (applyIntRdr [])

-- Code Duplication

-- ***      ******      *******      ****
-- runOptions :: [Option a] -> Option [a]
-- runIntRdrs :: [IntRdr a] -> IntRdr [a]

-- ***      ***********************      **************      ************           ****
-- runOptions = foldr (\a b -> bindOption a (\aa -> mapOption b (aa:))) (applyOption [])
-- runIntRdrs = foldr (\a b -> bindIntRdr a (\aa -> mapIntRdr b (aa:))) (applyIntRdr [])
-- run        = foldr (\a b -> (>>= a (\aa -> fmap (aa:) b)) 
run :: Monad m => [m a] -> m [a]
run = foldr (\a b -> (>>=) a (\aa -> fmap (aa:) b)) (pure [])
-- run = sequence

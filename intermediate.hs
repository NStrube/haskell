-- 20 Intermediate Haskell Exercises
-- https://blog.tmorris.net/posts/20-intermediate-haskell-exercises/
-- Solutions:
--   - https://gist.github.com/dmalikov/1493055

class Fluffy f where
  furry :: (a -> b) -> f a -> f b

-- Exercise 1
-- Relative Difficulty: 1
instance Fluffy [] where
  furry _ []      = []
  furry f (h:rst) = f h : furry f rst

-- Exercise 2
-- Relative Difficulty: 1
instance Fluffy Maybe where
  furry _ Nothing  = Nothing
  furry f (Just h) = Just $ f h

-- Exercise 3
-- Relative Difficulty: 5
instance Fluffy ((->) t) where
  -- f :: (->) t 
  -- furry :: (a -> b) -> f a -> f b
  -- f a == t -> a
  -- f b == t -> b
  -- t -> a -> b
  furry f t = f . t

newtype EitherLeft b a = EitherLeft (Either a b)
newtype EitherRight a b = EitherRight (Either a b)

-- Exercise 4
-- Relative Difficulty: 5
instance Fluffy (EitherLeft t) where
  -- furry :: (a -> b) -> f a -> f b
  -- f a == EitherLeft t a == Either a t
  -- f b == EitherLeft t b == Either b t
  furry _ (EitherLeft (Right r)) = EitherLeft $ Right r
  furry f (EitherLeft (Left l))  = EitherLeft $ Left $ f l

-- Exercise 5
-- Relative Difficulty: 5
instance Fluffy (EitherRight t) where
  -- furry :: (a -> b) -> f a -> f b
  -- f a == EitherRight t a == Either t a
  -- f b == EitherRight t b == Either t b
  furry _ (EitherRight (Left l))  = EitherRight $ Left l
  furry f (EitherRight (Right r)) = EitherRight $ Right $ f r

class Misty m where
  banana :: (a -> m b) -> m a -> m b
  unicorn :: a -> m a
  -- Exercise 6
  -- Relative Difficulty: 3
  -- (use banana and/or unicorn)
  furry' :: (a -> b) -> m a -> m b
  furry' f = banana $ unicorn . f

-- Exercise 7
-- Relative Difficulty: 2
instance Misty [] where
  banana = concatMap
  unicorn h = [h]

-- Exercise 8
-- Relative Difficulty: 2
instance Misty Maybe where
  banana _ Nothing  = Nothing
  banana f (Just h) = f h
  unicorn = Just

-- Exercise 9
-- Relative Difficulty: 6
instance Misty ((->) t) where
  -- banana :: (a -> m b) -> m a -> m b
  -- (a -> t -> b) -> (t -> a) -> (t -> b)
  banana f t h = f (t h) h
  unicorn h _ = h

-- Exercise 10
-- Relative Difficulty: 6
instance Misty (EitherLeft t) where
-- newtype EitherLeft b a = EitherLeft (Either a b)
-- newtype EitherRight a b = EitherRight (Either a b)
  -- banana :: (a -> m b) -> m a -> m b
  banana _ (EitherLeft (Right r)) = EitherLeft $ Right r
  banana f (EitherLeft (Left l))  = f l
  -- unicorn :: a -> m a
  unicorn = EitherLeft . Left

-- Exercise 11
-- Relative Difficulty: 6
instance Misty (EitherRight t) where
  banana _ (EitherRight (Left l)) =  EitherRight $ Left l
  banana f (EitherRight (Right r)) = f r
  unicorn = EitherRight . Right

-- Exercise 12
-- Relative Difficulty: 3
jellybean :: (Misty m) => m (m a) -> m a
jellybean = banana id

-- Exercise 13
-- Relative Difficulty: 6
-- Looked up solution
-- WHY DOES THIS WORK?????????
apple :: (Misty m) => m a -> m (a -> b) -> m b
-- apple a b = banana (\x -> banana (\y -> unicorn $ y x) b) a
apple = banana . flip furry'
-- banana :: (a -> m b) -> m a -> m b
-- unicorn :: a -> m a
-- furry' :: (a -> b) -> m a -> m b

-- Exercise 14
-- Relative Difficulty: 6
-- Looked up solution
moppy :: (Misty m) => [a] -> (a -> m b) -> m [b]
moppy l f = foldr (banana2 (:) . f) (unicorn []) l
-- banana2 :: (a -> b -> c) -> m a -> m b -> m c
-- (:) :: b -> c -> c
-- f :: (a -> m b)
-- foldFun :: b -> m c -> m c
-- f i :: m b
-- babana2 (:) . (f a) :: m c -> m c

-- Exercise 15
-- Relative Difficulty: 6
-- (bonus: use moppy)
sausage :: (Misty m) => [m a] -> m [a]
sausage = flip moppy id

-- Exercise 16
-- Relative Difficulty: 6
-- (bonus: use apple + furry')
banana2 :: (Misty m) => (a -> b -> c) -> m a -> m b -> m c
banana2 f a b = apple b $ furry' f a
-- furry' :: (a -> b) -> m a -> m b
-- apple  :: m a -> m (a -> b) -> m b
-- Looked up: banana2 = (flip apple .) . furry'
-- Not sure how this works?

-- Exercise 17
-- Relative Difficulty: 6
-- (bonus: use apple + banana2)
banana3 :: (Misty m) => (a -> b -> c -> d) -> m a -> m b -> m c -> m d
banana3 f a b c = apple c $ banana2 f a b

-- Exercise 18
-- Relative Difficulty: 6
-- (bonus: use apple + banana3)
banana4 :: (Misty m) => (a -> b -> c -> d -> e) -> m a -> m b -> m c -> m d -> m e
banana4 f a b c d = apple d $ banana3 f a b c

newtype State s a = State {
  state :: (s -> (s, a))
}

-- Exercise 19
-- Relative Difficulty: 9
instance Fluffy (State s) where
  -- furry :: (a -> b) -> f a -> f b
  furry f s = State $ \i -> let (s', i') = (state s) i -- NOTE: `state s i` alse works
                            in (s', f i')

-- Exercise 20
-- Relative Difficulty: 10
instance Misty (State s) where
  -- banana :: (a -> m b) -> m a -> m b
  banana f s = State $ \i -> let (s', i')   = (state s) i -- NOTE: `state s i` alse works
                                 f'         = state $ f i'
                             in f' s'
  -- unicorn :: a -> m a
  unicorn a = State $ \s -> (s, a)
